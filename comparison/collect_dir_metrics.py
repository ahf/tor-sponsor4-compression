#!/usr/bin/env python3

import sys
import os
import re

FILE = "warn.log"
old_p = re.compile('(.*) Received response from directory server \'(.*)\': (\d+) "(.*)" \(purpose: (\d+), response size: (\d+), data cells received: (\d+), data cells sent: (\d+), compression: (\d+)\)')
new_p = re.compile('(.*) Received response from directory server \'(.*)\': (\d+) "(.*)" \(purpose: (\d+), response size: (\d+), compression: (\d+)\)')

def inspect_file(path):
    path_tokens = path.split("/")

    relay_name = path_tokens[-1]
    relay_id   = int(relay_name[0:3])
    relay_type = relay_name[3:4]

    data = {
        'relay_id':   relay_id,
        'relay_type': relay_type,
        'dir_size': {},
        'http_status': {},
        'matches': 0,
    }

    print("Handling: %s" % relay_name)

    with open("%s/%s" % (path, FILE)) as f:
        for line in f:
            m = new_p.match(line)

            if m:
                remote_host     = m.group(2)
                http_status     = int(m.group(3))
                http_label      = m.group(4)
                purpose         = int(m.group(5))
                response_size   = int(m.group(6))
                compression     = int(m.group(7))
            else:
                m = old_p.match(line)

                if m:
                    remote_host     = m.group(2)
                    http_status     = int(m.group(3))
                    http_label      = m.group(4)
                    purpose         = int(m.group(5))
                    response_size   = int(m.group(6))
                    #data_cells_recv = int(m.group(7))
                    #data_cells_sent = int(m.group(8))
                    compression     = int(m.group(9))
                else:
                    #print("Invalid line: %s" % line)
                    continue

            if purpose not in data['dir_size']:
                data['dir_size'][purpose] = 0

            if 'sum' not in data['dir_size']:
                data['dir_size']['sum'] = 0

            if http_status not in data['http_status']:
                data['http_status'][http_status] = 0

            data['dir_size'][purpose] += response_size
            data['dir_size']['sum'] += response_size
            data['matches'] += 1
            data['http_status'][http_status] += 1

    return (relay_name, data)


if __name__ == '__main__':
    if len(sys.argv) < 4:
        print("Usage: %s <a> <b> <output.csv>" % sys.argv[0])
        sys.exit(1)

    r = {
        'a': {},
        'b': {}
    }

    for roots, dirs, files in os.walk(sys.argv[1]):
        if FILE not in files:
            continue

        key, value = inspect_file(roots)

        assert key not in r['a']
        r['a'][key] = value

    for roots, dirs, files in os.walk(sys.argv[2]):
        if FILE not in files:
            continue

        key, value = inspect_file(roots)

        assert key not in r['b']
        r['b'][key] = value

    a_keys = set(r['a'].keys())
    b_keys = set(r['b'].keys())

    common = a_keys.intersection(b_keys)

    with open(sys.argv[3], 'w') as output:
        for relay in sorted(common):
            a_purpose_keys = set(r['a'][relay]["dir_size"].keys())
            b_purpose_keys = set(r['b'][relay]["dir_size"].keys())

            common_purpose = a_purpose_keys.union(b_purpose_keys)

            print("%s vs. %s" % (r['a'][relay]['http_status'], r['b'][relay]['http_status']))

            for purpose in sorted(common_purpose, key = lambda x: -1 if x == "sum" else x):
                output.write("%s;%s;%s;%s;%s;%s\n" % (relay,
                                                      purpose,
                                                      r['a'][relay]["dir_size"].get(purpose, 0),
                                                      r['a'][relay]["matches"],
                                                      r['b'][relay]["dir_size"].get(purpose, 0),
                                                      r['b'][relay]["matches"]))
