/* vim: set sw=4 sts=4 et foldmethod=syntax : */

#ifndef GUARD_GZIP_H
#define GUARD_GZIP_H 1

void run_gzip_tests(int level, char *input_buffer, size_t input_buffer_size, char *output_buffer, size_t output_buffer_size);

#endif
