/* vim: set sw=4 sts=4 et foldmethod=syntax : */

#include <stdio.h>
#include <stdlib.h>

#include "config.h"
#include "tests.h"
#include "utilities.h"

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        fprintf(stderr, "Usage: %s <file>\n", argv[0]);
        return EXIT_FAILURE;
    }

    char *input_buffer = NULL;
    size_t input_buffer_size = 0;

    if (read_file(argv[1], &input_buffer, &input_buffer_size) != 0)
    {
        fprintf(stderr, "Unable to read file: %s\n", argv[1]);
        return EXIT_FAILURE;
    }

    /* We assume that our compression algorithm will always return <= our input
     * buffer :-) */
    char *output_buffer = malloc(input_buffer_size * sizeof(char));
    size_t output_buffer_size = input_buffer_size;

    run_tests(input_buffer, input_buffer_size, output_buffer, output_buffer_size);

    free(input_buffer);
    free(output_buffer);

    return EXIT_SUCCESS;
}
