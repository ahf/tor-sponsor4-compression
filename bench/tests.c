/* vim: set sw=4 sts=4 et foldmethod=syntax : */

#include <stdio.h>
#include <stdlib.h>

#include "tests.h"

#include "gzip.h"
#include "bzip2.h"
#include "lzma.h"
#include "zstd_test.h"
#include "lz4_test.h"

void run_tests(char *input_buffer, size_t input_buffer_size, char *output_buffer, size_t output_buffer_size)
{
    printf("Algorithm;Compression Level;Input Size;"
           "Compressed Size;Size Difference;Size Difference (percentage);"
           "Test Count;"
           "Compression Time;Decompression Time;"
           "Memory Allocated (compress);Max Allocation (compress);"
           "Memory Allocated (decompress);Max Allocation (decompress)\n");

    for (int level = 1; level <= 9; ++level)
        run_gzip_tests(level, input_buffer, input_buffer_size, output_buffer, output_buffer_size);

    for (int level = 1; level <= 9; ++level)
        run_bzip2_tests(level, input_buffer, input_buffer_size, output_buffer, output_buffer_size);

    for (int level = 1; level <= 9; ++level)
        run_lzma_tests(level, input_buffer, input_buffer_size, output_buffer, output_buffer_size);

    for (int level = 1; level <= 9; ++level)
        run_zstd_tests(level, input_buffer, input_buffer_size, output_buffer, output_buffer_size);

    run_lz4_tests(0, input_buffer, input_buffer_size, output_buffer, output_buffer_size);
}
