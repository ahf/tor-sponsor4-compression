/* vim: set sw=4 sts=4 et foldmethod=syntax : */

#ifndef GUARD_TESTS_H
#define GUARD_TESTS_H 1

/* run our various tests. */
void run_tests(char *input_buffer, size_t input_buffer_size, char *output_buffer, size_t output_buffer_size);

#endif
