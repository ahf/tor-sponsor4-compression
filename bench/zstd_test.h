/* vim: set sw=4 sts=4 et foldmethod=syntax : */

#ifndef GUARD_ZSTD_H
#define GUARD_ZSTD_H 1

void run_zstd_tests(int level, char *input_buffer, size_t input_buffer_size, char *output_buffer, size_t output_buffer_size);

#endif
