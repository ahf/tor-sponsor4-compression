/* vim: set sw=4 sts=4 et foldmethod=syntax : */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

int read_file(const char *filename, char **buffer, size_t *size)
{
    FILE *file = NULL;
    size_t file_size = 0;
    char *tmp = NULL;

    assert(buffer != NULL);
    assert(size != NULL);

    file = fopen(filename, "rb");

    if (file == NULL)
        return -1;

    if (0 != fseek(file, 0, SEEK_END))
        return -1;

    file_size = ftell(file);

    rewind(file);

    tmp = malloc(file_size * sizeof(char));

    fread(tmp, file_size, sizeof(char), file);

    if (fclose(file) != 0)
    {
        free(tmp);
        return -1;
    }

    *buffer = tmp;
    *size = file_size;

    return 0;
}

void current_timestamp(struct timespec *ts)
{
#ifdef __MACH__
    clock_serv_t cclock;
    mach_timespec_t mts;
    host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
    clock_get_time(cclock, &mts);
    mach_port_deallocate(mach_task_self(), cclock);
    ts->tv_sec = mts.tv_sec;
    ts->tv_nsec = mts.tv_nsec;
#else
    clock_gettime(CLOCK_REALTIME, ts);
#endif
}

void timestamp_diff(struct timespec *start, struct timespec *stop, struct timespec *result)
{
    if ((stop->tv_nsec - start->tv_nsec) < 0)
    {
        result->tv_sec = stop->tv_sec - start->tv_sec - 1;
        result->tv_nsec = stop->tv_nsec - start->tv_nsec + 1000000000;
    }
    else
    {
        result->tv_sec = stop->tv_sec - start->tv_sec;
        result->tv_nsec = stop->tv_nsec - start->tv_nsec;
    }
}
