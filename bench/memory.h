/* vim: set sw=4 sts=4 et foldmethod=syntax : */

#ifndef GUARD_MEMORY_H
#define GUARD_MEMORY_H 1

void *track_malloc(void *opaque, unsigned int items, unsigned int size);
void *track_malloc_int(void *opaque, int items, int size);
void *track_malloc_size_t(void *opaque, size_t items, size_t size);
void *track_malloc_zstd(void *opaque, size_t size);

void track_free(void *opaque, void *ptr);

void track_reset_counter(void);

size_t track_counter(void);
size_t track_max_allocation(void);

#endif
