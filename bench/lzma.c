/* vim: set sw=4 sts=4 et foldmethod=syntax : */

#include <assert.h>
#include <stdio.h>
#include <time.h>

#include <lzma.h>

#include "config.h"
#include "gzip.h"
#include "memory.h"
#include "utilities.h"

static void compress_lzma(int level, char *input_buffer, size_t input_buffer_size, char *output_buffer, size_t output_buffer_size, size_t *compressed_size)
{
    lzma_stream stream = LZMA_STREAM_INIT;
    lzma_allocator allocator;

    allocator.alloc = track_malloc_size_t;
    allocator.free = track_free;
    allocator.opaque = NULL;

    stream.allocator = &allocator;

    /* Input. */
    stream.avail_in = input_buffer_size;
    stream.next_in = (unsigned char *)input_buffer;

    /* Output. */
    stream.avail_out = output_buffer_size;
    stream.next_out = (unsigned char *)output_buffer;

    assert(lzma_easy_encoder(&stream, level, LZMA_CHECK_CRC64) == LZMA_OK);
    assert(lzma_code(&stream, LZMA_RUN) == LZMA_OK);
    assert(lzma_code(&stream, LZMA_FINISH) == LZMA_STREAM_END);
    lzma_end(&stream);

    *compressed_size = (char *)stream.next_out - output_buffer;
}

static void uncompress_lzma(char *input_buffer, size_t input_buffer_size, char *output_buffer, size_t output_buffer_size, size_t compressed_size)
{
    lzma_stream stream = LZMA_STREAM_INIT;
    lzma_allocator allocator;

    allocator.alloc = track_malloc_size_t;
    allocator.free = track_free;
    allocator.opaque = NULL;

    stream.allocator = &allocator;

    /* Input. */
    stream.avail_in = compressed_size;
    stream.next_in = (unsigned char *)output_buffer;

    /* Output. */
    stream.avail_out = input_buffer_size;
    stream.next_out = (unsigned char *)input_buffer;

    assert(lzma_stream_decoder(&stream, UINT64_MAX, LZMA_CONCATENATED) == LZMA_OK);
    assert(lzma_code(&stream, LZMA_RUN) == LZMA_OK);
    assert(lzma_code(&stream, LZMA_FINISH) == LZMA_STREAM_END);
    lzma_end(&stream);
}

void run_lzma_tests(int level, char *input_buffer, size_t input_buffer_size, char *output_buffer, size_t output_buffer_size)
{
    int n = TEST_COUNT;
    size_t compressed_size = 0;
    struct timespec start_compress, end_compress, result_compress;
    struct timespec start_uncompress, end_uncompress, result_uncompress;
    size_t compress_memory = 0, uncompress_memory = 0;
    size_t compress_max_allocation = 0, uncompress_max_allocation = 0;

    current_timestamp(&start_compress);
    while (n--)
    {
        compress_lzma(level, input_buffer, input_buffer_size, output_buffer, output_buffer_size, &compressed_size);
    }
    current_timestamp(&end_compress);

    compress_memory = track_counter();
    compress_max_allocation = track_max_allocation();
    track_reset_counter();

    n = TEST_COUNT;

    current_timestamp(&start_uncompress);
    while (n--)
    {
        uncompress_lzma(input_buffer, input_buffer_size, output_buffer, output_buffer_size, compressed_size);
    }
    current_timestamp(&end_uncompress);

    uncompress_memory = track_counter();
    uncompress_max_allocation = track_max_allocation();
    track_reset_counter();

    timestamp_diff(&start_compress, &end_compress, &result_compress);
    timestamp_diff(&start_uncompress, &end_uncompress, &result_uncompress);

    printf("lzma;%d;%zu;%zu;%zu;%.2f%%;%d;%lld.%.9ld;%lld.%.9ld;%zu;%zu;%zu;%zu\n",
            level,
            input_buffer_size,
            compressed_size,
            input_buffer_size - compressed_size,
            (double)compressed_size / (double)input_buffer_size * 100,
            TEST_COUNT,
            (long long)result_compress.tv_sec, result_compress.tv_nsec,
            (long long)result_uncompress.tv_sec, result_uncompress.tv_nsec,
            compress_memory,
            compress_max_allocation,
            uncompress_memory,
            uncompress_max_allocation);
}
