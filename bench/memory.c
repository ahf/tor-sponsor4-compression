/* vim: set sw=4 sts=4 et foldmethod=syntax : */

#include <assert.h>
#include <stdlib.h>

#include "memory.h"

static size_t counter;

static size_t max_allocation;
static size_t current_allocation;

void *track_malloc(void *opaque, unsigned int items, unsigned int size)
{
    (void)opaque;

    size_t allocation = size * items;
    void *memory = malloc(sizeof(size_t) + allocation);

    counter += allocation;
    current_allocation += allocation;

    if (current_allocation > max_allocation)
        max_allocation = current_allocation;

    *(size_t *)memory = allocation;

    return memory + sizeof(size_t);
}

void track_free(void *opaque, void *ptr)
{
    (void)opaque;

    if (ptr == NULL)
        return;

    void *real_ptr = ptr - sizeof(size_t);
    size_t allocation = *(size_t *)real_ptr;

    current_allocation -= allocation;

    free(real_ptr);
}

void *track_malloc_int(void *opaque, int items, int size)
{
    return track_malloc(opaque, (unsigned int)items, (unsigned int)size);
}

void *track_malloc_size_t(void *opaque, size_t items, size_t size)
{
    return track_malloc(opaque, (unsigned int)items, (unsigned int)size);
}

void *track_malloc_zstd(void *opaque, size_t size)
{
    return track_malloc(opaque, 1, size);
}

void track_reset_counter(void)
{
    /* Check if there was data that we didn't handle. */
    assert(current_allocation == 0);

    counter = 0;
    max_allocation = 0;
    current_allocation = 0;
}

size_t track_counter(void)
{
    return counter;
}

size_t track_max_allocation(void)
{
    return max_allocation;
}
