/* vim: set sw=4 sts=4 et foldmethod=syntax : */

#include <assert.h>
#include <stdio.h>
#include <time.h>

#include <bzlib.h>

#include "config.h"
#include "gzip.h"
#include "memory.h"
#include "utilities.h"

static void compress_bzip2(int level, char *input_buffer, size_t input_buffer_size, char *output_buffer, size_t output_buffer_size, size_t *compressed_size)
{
    bz_stream stream;

    stream.bzalloc = track_malloc_int;
    stream.bzfree = track_free;
    stream.opaque = NULL;

    /* Input. */
    stream.avail_in = input_buffer_size;
    stream.next_in = input_buffer;

    /* Output. */
    stream.avail_out = output_buffer_size;
    stream.next_out = output_buffer;

    assert(BZ2_bzCompressInit(&stream, level, 0, 0) == BZ_OK);
    assert(BZ2_bzCompress(&stream, BZ_FINISH) == BZ_STREAM_END);
    assert(BZ2_bzCompressEnd(&stream) == BZ_OK);

    *compressed_size = stream.next_out - output_buffer;
}

static void uncompress_bzip2(char *input_buffer, size_t input_buffer_size, char *output_buffer, size_t output_buffer_size, size_t compressed_size)
{
    bz_stream stream;

    stream.bzalloc = track_malloc_int;
    stream.bzfree = track_free;
    stream.opaque = NULL;

    /* Input. */
    stream.avail_in = compressed_size;
    stream.next_in = output_buffer;

    /* Output. */
    stream.avail_out = input_buffer_size;
    stream.next_out = input_buffer;

    assert(BZ2_bzDecompressInit(&stream, 0, 0) == BZ_OK);
    assert(BZ2_bzDecompress(&stream) == BZ_STREAM_END);
    assert(BZ2_bzDecompressEnd(&stream) == BZ_OK);
}

void run_bzip2_tests(int level, char *input_buffer, size_t input_buffer_size, char *output_buffer, size_t output_buffer_size)
{
    int n = TEST_COUNT;
    size_t compressed_size = 0;
    struct timespec start_compress, end_compress, result_compress;
    struct timespec start_uncompress, end_uncompress, result_uncompress;
    size_t compress_memory = 0, uncompress_memory = 0;
    size_t compress_max_allocation = 0, uncompress_max_allocation = 0;

    current_timestamp(&start_compress);
    while (n--)
    {
        compress_bzip2(level, input_buffer, input_buffer_size, output_buffer, output_buffer_size, &compressed_size);
    }
    current_timestamp(&end_compress);

    compress_memory = track_counter();
    compress_max_allocation = track_max_allocation();
    track_reset_counter();

    n = TEST_COUNT;

    current_timestamp(&start_uncompress);
    while (n--)
    {
        uncompress_bzip2(input_buffer, input_buffer_size, output_buffer, output_buffer_size, compressed_size);
    }
    current_timestamp(&end_uncompress);

    uncompress_memory = track_counter();
    uncompress_max_allocation = track_max_allocation();
    track_reset_counter();

    timestamp_diff(&start_compress, &end_compress, &result_compress);
    timestamp_diff(&start_uncompress, &end_uncompress, &result_uncompress);

    printf("bzip2;%d;%zu;%zu;%zu;%.2f%%;%d;%lld.%.9ld;%lld.%.9ld;%zu;%zu;%zu;%zu\n",
            level,
            input_buffer_size,
            compressed_size,
            input_buffer_size - compressed_size,
            (double)compressed_size / (double)input_buffer_size * 100,
            TEST_COUNT,
            (long long)result_compress.tv_sec, result_compress.tv_nsec,
            (long long)result_uncompress.tv_sec, result_uncompress.tv_nsec,
            compress_memory,
            compress_max_allocation,
            uncompress_memory,
            uncompress_max_allocation);
}
