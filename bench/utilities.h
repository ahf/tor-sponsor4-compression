/* vim: set sw=4 sts=4 et foldmethod=syntax : */

#ifndef GUARD_UTILITIES_H
#define GUARD_UTILITIES_H 1

#include <time.h>

int read_file(const char *filename, char **buffer, size_t *size);
void current_timestamp(struct timespec *ts);
void timestamp_diff(struct timespec *start, struct timespec *stop, struct timespec *result);

#endif
