/* vim: set sw=4 sts=4 et foldmethod=syntax : */

#ifndef GUARD_CONFIG_H
#define GUARD_CONFIG_H 1

#ifndef TEST_COUNT
#  define TEST_COUNT 1000
#endif

#endif
