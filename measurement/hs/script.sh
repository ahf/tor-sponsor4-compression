#!/bin/sh

while true ; do
    echo "HUP'ing"
    kill -HUP $(cat run/tor.pid)
    sleep $1
done
